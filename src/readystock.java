class readystock extends kue{
    private double berat;
    public double harga;
    public double jumlah;
    //mengambil nama, harga, dan jumlah
    public readystock (String name, double price, double total) {
        super(name,price);
        setHarga(price);
        setJumlah(total);
    }
    
    private void setJumlah(double total) {
        jumlah = total;
    }
    
    public double getJumlah() {
        return jumlah;
    }
    
    public void setHarga(double price) {
        harga = price;
    }
    
    public double getHarga() {
        return harga;
    }
    
    //menghitung harga kue yang sudah readystock
    @Override
    public double hitungHarga() {
        return harga*jumlah*2;
    }
    
    @Override
    public double Berat() {
        return berat;
    }
    
    @Override
    public double Jumlah() {
        return jumlah;
    }
 
}