class pesanan extends kue{
    private double berat;
    public double harga;
    public double jumlah;
    //mengambil nama, harga, dan berat
    public pesanan (String name, double price, double weight) {
        super(name,price);
        setHarga(price);
        setBerat(weight);
    }
    
    private void setBerat(double weight) {
        berat = weight;
    }
    
    public double getBerat() {
        return berat;
    }
    
    public void setHarga(double price) {
        harga = price;
    }
    
    public double getHarga() {
        return harga;
    }
    
    //method untuk harga menghitung kue yang pesanan
    @Override
    public double hitungHarga() {
        return harga*berat;    
    }

    @Override
    public double Berat() {
        return berat;
    }

    @Override
    public double Jumlah() {
    return jumlah;
    }

    
    
}